%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_data).

-export(
   [app_hash/1,
    apply_shift_to_now/1,
    apply_shift_to_timestamp/2,
    construct/1,
    grant_fee_deposit/1,
    money_supply/1,
    now/1,
    shift_to_timestamp/2]).

-compile({parse_transform, dynarec}).

-include_lib("include/ercoin.hrl").

-spec app_hash(data()) -> binary().
app_hash(Data) ->
    AccountsHash =
        case gb_merkle_trees:root_hash(Data#data.accounts) of
            undefined ->
                %% This means that there are no accounts anymore, which is an uninteresting case, but implementing this eases testing.
                <<0:256>>;
            AccountsHash2 ->
                AccountsHash2
        end,
    OtherDataHash =
        ?HASH(
           <<(Data#data.protocol),
             (Data#data.previous_timestamp):4/unit:8,
             (Data#data.timestamp):4/unit:8,
             (Data#data.last_epoch_end):4/unit:8,
             (ercoin_epoch:stage_no(Data#data.epoch_stage)),
             (Data#data.fee_deposit):8/unit:8,
             (ercoin_validators:current_hash(Data#data.validators))/binary,
             (ercoin_validators:future_hash(Data#data.future_validators))/binary,
             (Data#data.fresh_txs_hash)/binary>>),
    ?HASH(<<AccountsHash/binary, OtherDataHash/binary>>).

-spec grant_fee_deposit(data()) -> data().
%% @doc Dispose deposit, assuming that the time for it has come.
grant_fee_deposit(
  Data=#data{
          validators=Validators,
          fee_deposit=FeeDeposit,
          timestamp=Timestamp,
          last_epoch_end=LastEpochEnd}) ->
    RealEpochLength = Timestamp - LastEpochEnd,
    SharesValidators =
        [{VP, PK} || {PK, <<VP, _/binary>>} <- gb_merkle_trees:to_orddict(Validators)],
    ValidatorsRewards = 'hare-niemeyer':apportion(SharesValidators, FeeDeposit),
    Data1 =
        lists:foldl(
          fun ({PK, Reward}, DataAcc) ->
                  case ercoin_validators:absencies(PK, Validators) < RealEpochLength div 3 of
                      true ->
                          ercoin_account:add_balance(PK, Reward, DataAcc);
                      false ->
                          DataAcc
                  end
          end,
          Data,
          ValidatorsRewards),
    Data1#data{fee_deposit=0}.

-spec apply_shift_to_timestamp(ercoin_timestamp:timestamp(), data()) -> data().
apply_shift_to_timestamp(Timestamp, Data) ->
    {NewData, _} = shift_to_timestamp(Timestamp, Data),
    NewData.

-spec shift_to_timestamp(ercoin_timestamp:timestamp(), data()) -> {data(), list(ercoin_event:event())}.
shift_to_timestamp(NewTimestamp, Data=#data{epoch_length=EpochLength, fresh_txs=FreshTxs, timestamp=OldTimestamp}) ->
    NewFreshTxs = lists:dropwhile(fun ({TxTimestamp, _}) -> TxTimestamp < NewTimestamp - EpochLength end, FreshTxs),
    shift_accounts_to_timestamp(Data#data{timestamp=NewTimestamp, previous_timestamp=OldTimestamp, fresh_txs=NewFreshTxs}).

-spec now(data()) -> ercoin_timestamp:timestamp().
now(Data=#data{now_fun=F}) ->
    apply(F, [Data]).

-spec apply_shift_to_now(data()) -> data().
apply_shift_to_now(Data) ->
    apply_shift_to_timestamp(?MODULE:now(Data), Data).

-spec shift_accounts_to_timestamp(data()) -> {data(), list(ercoin_event:event())}.
shift_accounts_to_timestamp(Data=#data{accounts=AccountsTree, timestamp=Timestamp, fee_deposit=FeeDeposit}) ->
    #{fee_per_account_day := FPerAccountDay} = ercoin_fee:fees(Data),
    {NewAccountsTree, CollectedFees, Events} = shift_accounts_tree_to_timestamp(AccountsTree, Timestamp, FPerAccountDay),
    {Data#data{accounts=NewAccountsTree, fee_deposit=FeeDeposit + CollectedFees}, Events}.

-spec shift_accounts_tree_to_timestamp(gb_merkle_trees:tree(), ercoin_timestamp:timestamp(), money()) -> {gb_merkle_trees:tree(), money(), list(ercoin_event:event())}.
%% @doc Unlock and extend/remove accounts, returning a new tree and sum of account fees.
shift_accounts_tree_to_timestamp(AccountsTree, Timestamp, FPerAccountDay) ->
    gb_merkle_trees:foldr(
      fun ({Address, AccountBin}, {TreeAcc, FeeAcc, EventsAcc}) ->
              Account = #account{valid_until=ValidUntil, balance=Balance, locked_until=LockedUntil} = ercoin_account:deserialize({Address, AccountBin}),
              Account1 =
                  case LockedUntil < Timestamp of
                      true ->
                          Account#account{locked_until=none};
                      false ->
                          Account
                  end,
              case Account1 of
                  #account{valid_until=ValidUntil, balance=Balance} when ValidUntil < Timestamp, Balance =:= 0 ->
                      {gb_merkle_trees:delete(Address, TreeAcc), FeeAcc, EventsAcc};
                  #account{valid_until=ValidUntil, balance=Balance} when ValidUntil < Timestamp ->
                      Fee = min(Balance, ?ACCOUNT_EXTENSION_DAYS * FPerAccountDay),
                      ExtensionTime =
                          case Fee of
                              0 ->
                                  ?ACCOUNT_EXTENSION_DAYS;
                              _ ->
                                  ercoin_fee:div_ceil(Fee * 3600 * 24, FPerAccountDay)
                          end,
                      NewAccount = Account1#account{balance=Balance - Fee, valid_until=ValidUntil + ExtensionTime},
                      {ercoin_account:put(NewAccount, TreeAcc),
                       FeeAcc + Fee,
                       [#{type => <<"account.extended">>,
                          attributes =>
                              #{<<"address">> => Address,
                                <<"fee">> => integer_to_binary(Fee),
                                <<"time">> => integer_to_binary(ExtensionTime)}}
                        |EventsAcc]};
                  #account{} ->
                      {case Account1 of
                           Account ->
                               TreeAcc;
                           _ ->
                               ercoin_account:put(Account1, TreeAcc)
                       end,
                       FeeAcc,
                       EventsAcc}
              end
      end,
      {AccountsTree, 0, []},
      AccountsTree).

-spec money_supply(data()) -> non_neg_integer().
money_supply(#data{accounts=Accounts, fee_deposit=FeeDeposit}) ->
    AccountsBalance =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, Sum) ->
                  #account{balance=Balance} = ercoin_account:deserialize(AccountSerialized),
                  Balance + Sum
          end,
          0,
          Accounts),
    AccountsBalance + FeeDeposit.

-spec construct(map()) -> data().
%% @doc Construct data from provided option map.
%% Options are not mandatory and generally correspond to data fields, with special case of <code>accounts_opts</code> which needs to be a list of account construction options. If timestamp fields are absent, they are set to current timestamp.
%% @see ercoin_account:construct/1
construct(Opts) ->
    Now = ercoin_timestamp:now(),
    TimestampFields =
        maps:from_list([{Field, maps:get(Field, Opts, Now)} || Field <- [timestamp, previous_timestamp, last_epoch_end]]),
    Data1 =
        maps:fold(
          fun (Key, Value, DataAcc) ->
                  case Key of
                      accounts_opts ->
                          Accounts = ercoin_account:accounts_from_option_maps(Value),
                          ercoin_data:set_value(accounts, Accounts, DataAcc);
                      _ ->
                          ercoin_data:set_value(Key, Value, DataAcc)
                  end
          end,
          #data{},
          maps:merge(Opts, TimestampFields)),
    case gb_merkle_trees:size(Data1#data.validators) of
        0 ->
            Data1#data{validators=ercoin_validators:draw(Data1)};
        _ ->
            Data1
    end.
