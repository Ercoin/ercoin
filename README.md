# Ercoin

Ercoin is a proof of stake cryptocurrency written in [Erlang](https://www.erlang.org) using [Tendermint](https://tendermint.com) and released under Apache License 2.0.

[Issues are tracked on GitLab.](https://gitlab.com/Ercoin/ercoin/issues) Contributions are welcome. Before submitting a patch, read [contributing guidelines](CONTRIBUTING.md).

## Development installation

1. Install [Tendermint](https://tendermint.com) (version 0.33.0).
2. Install [Erlang](https://www.erlang.org) (21 is the minimum version).
3. Install [libsodium](https://libsodium.org) (1.0.12 is the minimum version; when using a package manager, you may need to install a separate package containing development files).
4. Clone the Ercoin’s repository and enter the created directory.
5. `make app`
6. Initialize the chain (see below).
7. `make run`
8. In another window, run `tendermint node --home ~/.ercoin/`

### Initializing the chain

You can either generate a custom test network or download an existing genesis file.

#### Generating a custom testnet

You need at least two accounts: one locked which allows initial validators to be drawn and one unlocked. Create addresses and corresponding private keys and save data specification to a file, like this:

```erlang
#{accounts_opts =>
      [#{%% Addresses can be written either as Erlang binaries or as Base64 strings.
         address => "OeFZLmb5xrqc60U+/xztZxnVj3SSpKmIWzlz5U83oJU=",
         valid_for => 3600 * 24,
         balance => 100000000000000},
       #{address => "Hdu8UaISmnbiFzTtjagRNJS+FxKi72aUCW0RRsaDIiU=",
         valid_for => 3600 * 24 * 365,
         locked_for => 3600 * 24 * 365,
         balance => 1}],
 epoch_length => 3600}.
```

Assuming that the file is named `testnet-config.script`, run `make init data_opts='"testnet-config.script"'`. You can also pass the option map inline in the `data_opts` argument (no double quotes are then needed) or from standard input (when `data_opts` is not provided).

If you enter a low value of `epoch_length`, you may also want to lower the value of `create_empty_blocks_interval` in `config.toml`.

See documentation of `ercoin_genesis:testnet_data/1` for details.

#### Initialization using an existing genesis file

Run `make init` with the following environment variables:

* `GENESIS_URL` — an URL of the `genesis.json` file.
* `GENESIS_SHA256` — SHA-256 sum of the `genesis.json` file. Non-mandatory, used to prevent tampering.

## Persistence and initial state

ABCI server dumps current state to `$ERCOIN_HOME` (with fallback to `~/.ercoin`) at the beginning of each epoch. The initial state, encoded using Base64, is contained as `app_state` in the `genesis.json` file in Tendermint config directory.

State is serialized using [Erlang’s External Term Format](http://erlang.org/doc/apps/erts/erl_ext_dist.html).

## Docker integration

To run the project using [Docker Compose](https://docs.docker.com/compose/), execute the following steps, assuming that data options for testnet are located in `testnet-config.script`:

1. `docker-compose build`
2. `docker volume create --name=ercoin_home`
3. `docker-compose run --no-deps tendermint init`
4. `docker-compose run abci-server make testnet < testnet-config.script`
5. `docker-compose up`

---

Docker images for tags and branches are automatically built in GitLab CI (unless tests are failing) and available in the [container registry](https://gitlab.com/Ercoin/ercoin/container_registry). For example, to pull an image generated from `master` branch:

```
docker pull registry.gitlab.com/ercoin/ercoin:master
```

`latest` tag points to `master`.

## Technical documentation

The information presented below is not a specification and does not cover every technical detail. If there is a discrepancy between the documentation and the code, it is possible that the code will prevail.

All numbers are big endian.

Keys are Ed25519.

The smallest possible unit is microercoin (µERN). An Ercoin node operates only on microercoins and treats it as a basic unit.

Timestamps are expressed in Unix time format.

Addresses from which transactions are performed are derived from signatures.

### Transfer transaction format

* Transaction type (0, 1 byte).
* Timestamp (4 bytes).
* To address (32 bytes).
* Value (8 bytes).
* Message length (1 byte).
* Message.
* Signature of all the previous fields.

### Lock transaction format

* Transaction type (2, 1 byte).
* Locked until timestamp (4 bytes).
* Intended validator public key (32 bytes)
* Signature of all the previous fields.

### Vote transaction format

* Transaction type (3, 1 byte).
* Timestamp (4 bytes).
* Choices.
* Signature of all the previous fields.

### Choices format

* Price per tx (8 bytes).
* Price per 256 bytes per tx (8 bytes).
* Price of storing an account for 1 day (8 bytes).
* Protocol version (1 byte).

### Burn transaction format

* Transaction type (4, 1 byte).
* Timestamp (4 bytes).
* Value (8 bytes).
* Message length (1 byte).
* Message.
* Signature of all the previous fields.

### Signature format

* Number of keys (at least one) that signed the message (1 byte).
* A sequence of public keys (32 bytes each).
* A Merkle path (may be empty) proving that either the public key (when singular key is used) or the hash of concatenated public keys is contained in the address.
* A sequence of Ed25519 signatures corresponding to the public keys, signing the message concatenated with previous signature fields, up to and including Merkle path (64 bytes each).

#### Merkle path format

* Non-empty sequence of the following:
  * A value indicating whether the following value needs to be appended (1) or prepended (0) to obtain next hash (1 byte).
  * Value contributing to the hash.

### Tags

The following tags are set for successfully delivered transactions:

* From (Base64-encoded address).
* Fee.
* Message (Base64-encoded) — if present.
* To (Base64-encoded address) — if present.
* Type.
* Value.

### Account serialization format

Key:
* Address (32 bytes).

Value:

* Valid until (4 bytes).
* Balance (8 bytes).
* Locked until (optional, 4 bytes).
* Intended validator public key (32 bytes) — present if the “locked until” field is present.

### Error codes

* 0 — ok.
* 1 — bad request.
* 2 — not found.
* 3 — forbidden.
* 4 — insufficient funds.
* 5 — invalid timestamp.
* 6 — already executed.

### Application hash

The following Merkle tree is formed when calculating an application hash:

* Accounts’ root hash.
* Other data hash:
  * Protocol number.
  * Timestamp of previous block.
  * Timestamp of current block.
  * Timestamp of the last block in previous epoch.
  * Epoch stage number (1-indexed).
  * Fee deposit.
  * Hash of validators for the next block.
  * Future validators’ hash.
  * Fresh transactions’ hash.

### State machine mechanism

#### Time passing

Blocks are divided into epochs, during which validator set is immutable. Epochs are divided into stages. Stage progress is done sequentially at the end of `BeginBlock` and depends mainly on the block timestamp in relation to the end of the last epoch.

For a specification of conditions for achieving particular stages and lists of actions that are taken when stages are achieved, consult the functions `should_we_step/1` and `stage_actions/1` from the `ercoin_epoch` module.

Other actions, like removing expired accounts, are performed every block.

#### Transactions

Validators are responsible for providing replay protection. Transactions which provide timestamps are valid only for the epoch length. Due to [issue #2840 in Tendermint](https://github.com/tendermint/tendermint/issues/2840), timestamps from the near future (up to and including epoch length) are also allowed.

Transfer transactions modify accounts’ balances. They may not be performed from a locked account, but they can transfer funds to a locked account.

Lock transactions lock accounts and specify intended validator public keys. They may be performed by locked accounts. Note that while it is technically possible for a complex address to become a validator, only an address being a public key will be able to sign blocks.

Vote transactions can be performed by validators or yielded future validators to vote for fee sizes and protocol changes.

#### Accounts

Accounts are initially created by transfer transactions, with “valid until” field set to current timestamp. If account’s validity ends and it has non-zero balance, it is automatically extended for a month or for its total balance, whichever value results in lower fee.

#### Fees

Fees are charged for extending accounts and for transactions, except vote transactions. Applicable fee amounts are calculated as medians of weighted validators’ votes, with higher middle values chosen if sum of weights is even.

Fees are put into fee deposit. At the end of every epoch, it is divided between validators proportionally to their voting power. If a validator has at least 1⁄3 absencies in the passing epoch, its reward is destroyed.

#### Validators’ drawing

Drawing of validators is performed with scores proportional to:

* account balance;
* time for which an account will be locked, counting from the estimated start of the second next epoch, with a cap of three years.

Partial seats are distributed randomly, proportionally to the remainders.

If there are no voters, the current set of validators is extended onto the next epoch.

### Initial data

SHA256 sum of [the generated genesis file](https://ercoin.tech/genesis.json) is `e62f25c883e33a1920cda9b147540734982fc018b5e079a01bbd47528e3350fd`.

Original rules for obtaining initial data are listed below. After the distribution period ended, some issues were revealed and a voting has been scheduled to resolve them and make a decision about the launch date.

Initial amount of coins will be distributed proportionally to the associated amounts of [BlackCoin](https://blackcoin.org) burnt in a selected time window. To participate in the distribution, create an output with the following script:

* 106 — [`OP_RETURN`](https://en.bitcoin.it/wiki/OP_RETURN) opcode, 1 byte.
* one of the following:
  - 76 — `OP_PUSHDATA1`
  - 77 — `OP_PUSHDATA2`
  - 78 — `OP_PUSHDATA4`
* length of the rest of the script — 1, 2 or 4 bytes, depending on the previous field.
* string “<code>Ercoin </code>” — 7 bytes.
* length of the “locked for” field — 0 to 4, 1 byte. (Tip: use 0 unless the account is going to be locked).
* locked for (in seconds) — 0 to 4 bytes, depending on the previous field. All values are valid, but a cap of 3.5 years is applied in regard to created account.
* intended validator public key — 32 bytes, present if the “locked until” field is greater than 0.
* signature of all the fields starting from “<code>Ercoin </code>”.

Transaction needs to be included in a BlackCoin block with a block timestamp from 2019-11-08 00:00:00 UTC to 2019-12-07 23:59:59 UTC. All accounts participating in the initial distribution will be initially valid for 3 days. If an account has multiple associated outputs, then “locked for” and “intended validator public key” will be picked from the output with highest “locked for”, with order of inclusion used as a secondary method of comparison. Amount of ercoins will be proportional to the sum of blackcoins burnt in all outputs, with total number of ERNs created being 2.6 billion. The minimum required burnt amount per Ercoin address is 3 BLK.

Initial validators will be drawn using ordinary rules, with data timestamp for obtaining entropy being the declared timestamp for the burning end plus 120. Future validators will be able to put vote transactions into BlackCoin blockchain, analogously to the genesis transaction described above, in blocks with a timestamp starting from a week to a day before genesis timestamp (which will be 2019-12-15 00:00:00 UTC if everything goes well). Transactions will be checked and applied in the order of inclusion and it will be the last step of creating genesis data.

#### Voting rules

A rescheduled period of fee voting will be used to make some decisions. Each decision will be binary (yes/no), encoded using one bit of the protocol field (it will not affect the actual protocol number in production), starting from the most significant bit. The following questions will be answered:

1. Do you accept the described method of proceeding (including this set of questions)?
2. Should we wait until Tendermint 0.33 is released? If so, then launch will happen a week after that release happens, rounded up to midnight (UTC). Fee voting will end a day before the launch. If the decision is made to not wait, then the launch time will likely need a separate voting.
3. Should we accept transactions confirmed shortly after the distribution deadline, but with creation timestamps before the deadline? To be precise: there is only one such transaction. If yes, then problematic outputs will have the missing part of the fee (amount that would be sufficient to confirm the transaction quickly, i.e. to make it relayed by the BlackCoin Original wallet) deducted from their value.
4. Should we accept transactions containing burn outputs resembling unlocked Ercoin accounts, but with incomplete (invalid) signatures? If yes, then problematic outputs will have the missing part of the fee (amount which would be additionally needed if a transaction contained a complete signature in case of a simple address) deducted from their value. Note that there are no transaction outputs for locked accounts and which contain incomplete signatures.

Remaining bits should be set to zero. Voting on points 1, 3 and 4 ends on 2020-01-19 00:00:00 UTC. Initial decision on point 2 is made at the same time, but it may be changed (by putting fresher votes) until the release of Tendermint 0.33 happens.

Validators will be drawn using the original rules (including original distribution). Note that problematic burn transactions do not involve locked accounts.

#### Voting outcome

All questions were answered positively. Tendermint 0.33 was released on 2020-01-15 (UTC), hence the launch has been scheduled to 2020-01-23 00:00:00 (UTC).

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
