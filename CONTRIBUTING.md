# Ercoin contributing guidelines

## Commit messages

Don’t refer to issue numbers in commit messages. If needed, use full issue URLs.

## Documentation

Code is documented using EDoc. Writing function specifications is desired.

## Testing

If it is feasible, write tests. Testing is done mainly using [Triq](https://triq.gitlab.io).

Code must pass the `make check` invocation (which composes `test` and `dialyze` targets).

## Code style

Common Erlang programming style recommendations apply. Exceptions and other rules are listed below.

There is no line length limit. Use common sense to make code readable.

Except binaries, put spaces after commas.

When introducing line breaks into statements, attempt to stick to the principle of least indentation. For example, the following code samples have proper line breaks:

```erlang
foo(#bar{baz=qux, quux=quuux}}) ->
    quuuux.

foo(
  #bar{baz=qux, quux=quuux}) ->
    quuuux.

foo(
  #bar{
     baz=qux,
     quux=quuux}) ->
    quuuux.
```

The following code samples have line breaks put incorrectly:

```erlang
foo(#bar{baz=qux,
         quux=quuux}) ->
    quuuux.

foo(#bar{
       baz=qux,
       quux=quuux}) ->
    quuuux.

foo(
  #bar{baz=qux,
       quux=quuux}) ->
    quuuux.
```

## Committers

Every external contribution must have an associated merge commit.

Sign your commits with PGP.
