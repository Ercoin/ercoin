%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_entropy).

-export([reliable_entropy/1]).
-export([simple_entropy/1]).

-include_lib("include/ercoin.hrl").

-spec reliable_entropy(data()) -> binary().
%% @doc Obtain entropy for data in such way that should not be subject to manipulation.
reliable_entropy(#data{timestamp=Timestamp, epoch_length=EpochLength}) ->
    %% Drawing of validators waits half of the epoch before yielding result, so waiting for 1/100th of the epoch
    %% before obtaining entropy (to further ensure that it will not be manipulated) should leave us with plenty of time
    %% to wait in case of eventual beacon downtime.
    nist_beacon:pulse(Timestamp + EpochLength div 100).

-spec simple_entropy(data()) -> binary().
%% @doc Obtain pseudoentropy for data, possibly for development purposes.
%% Will be cheap and quick to generate but don’t expect it to be secure.
simple_entropy(#data{height=Height, timestamp=Timestamp}) ->
    ?HASH(<<Height:4/unit:8, Timestamp:4/unit:8>>).
