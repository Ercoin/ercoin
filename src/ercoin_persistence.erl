%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_persistence).
-behaviour(gen_server).

-include_lib("include/ercoin.hrl").

%% API.
-export([start_link/0]).
-export([current_data/0]).
-export([dump_data_async/1]).
-export([data_dir/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
}).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

data_dir() ->
    case os:getenv("ERCOIN_HOME") of
        false ->
            Home = os:getenv("HOME"),
            filename:absname_join(Home, ".ercoin");
        ErcoinHome ->
            ErcoinHome
    end.

-spec current_data() -> {ok, term()} | none.
current_data() ->
    DataPath = filename:absname_join(data_dir(), "current_data.bin"),
    case file:read_file(DataPath) of
        {ok, DataBin} ->
            {ok, binary_to_term(DataBin)};
        {error, enoent} ->
            none
    end.

%% @doc Dump data asynchronously.
%% This function converts data to binary (with emptied mempool_data) and schedules writing it to disc.
-spec dump_data_async(term()) -> ok.
dump_data_async(Data) ->
    %% We create a binary here to not copy large term between processes.
    DataBin = term_to_binary(Data#data{mempool_data=undefined}),
    gen_server:cast(?MODULE, {store_data_bin, DataBin}).

%% gen_server.

init([]) ->
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast({store_data_bin, DataBin}, State) ->
    DataPath = filename:absname_join(data_dir(), "current_data.bin"),
    ok = write_atomically(DataPath, DataBin),
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

-spec write_atomically(file:name_all(), binary()) -> ok | {error, term()}.
%% @doc Write file atomically, with obtaining lock.
write_atomically(Filename, Bytes) ->
    TmpFilename = [Filename, ".tmp"],
    case file:open(TmpFilename, [write, exclusive]) of
        {ok, F} ->
            ok = file:write(F, Bytes),
            file:rename(TmpFilename, Filename);
        {error, eexist} ->
            {error, temporary_file_already_exists}
    end.
