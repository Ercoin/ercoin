%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_sig_tests).

-include_lib("triq/include/triq.hrl").

-export(
   [address_secret/0,
    address/0,
    invalid_sign/2,
    invalid_sign_detached/2,
    sign/2,
    sign_detached/2]).

-export_type(
   [secret/0]).

-opaque secret() :: {list({PK :: <<_:256>>, SK :: <<_:512>>}), merkle_proofs:path()}.

msg() ->
    binary().

pk_sk() ->
    domain(
      pk_sk,
      fun (Self, _) ->
              #{public := Public, secret := Private} = enacl:sign_keypair(),
              {Self, {Public, Private}}
      end,
      fun (Self, Value) -> {Self, Value} end).

secret() ->
    ?SUCHTHAT(
       {PKsSKs, _},
       {non_empty(list(pk_sk())), merkle_proofs_tests:'merkle-path'()},
       length(PKsSKs) =< 255).

address_secret() ->
    ?LET(
       Secret={PKsSKs, Path},
       secret(),
       begin
           CombinedPKs =
               case PKsSKs of
                   [{PK, _}] ->
                       PK;
                   _ ->
                       crypto:hash(sha256, (<<PK || {PK, _} <- PKsSKs>>))
               end,
           Address = merkle_proofs:fold({CombinedPKs, Path}),
           {Address, Secret}
       end).

address() ->
    binary(32).

msg_sig() ->
    ?LET(
       {Msg, Secret},
       {msg(), secret()},
       {Msg, sign_detached(Msg, Secret)}).

-spec invalid_path_sign_detached(ercoin_sig:msg(), secret()) -> triq_dom:domain(binary()).
invalid_path_sign_detached(Msg, Secret={PKsSKs, _}) ->
    ?LET(
       InvalidPathBin,
       merkle_proofs_tests:'invalid-path-bin'(),
       begin
           PKsBin = <<PK || {PK, _} <- PKsSKs>>,
           ToSign = <<Msg/binary, (length(PKsSKs)), PKsBin/binary, InvalidPathBin/binary>>,
           RawSigs = raw_sign_detached(ToSign, Secret),
           <<ToSign/binary, RawSigs/binary>>
       end).

-spec no_keys_sign_detached(ercoin_sig:msg(), secret()) -> binary().
no_keys_sign_detached(_, {_, Path}) ->
    <<0, (merkle_proofs:'serialize-path'(Path))/binary>>.

-spec invalid_sign_detached(ercoin_sig:msg(), secret()) -> triq_dom:domain(binary()).
invalid_sign_detached(Msg, Secret) ->
    frequency(
      [{20, binary()},
       {10, ercoin_gen:mutate_bin(sign_detached(Msg, Secret))},
       {5, invalid_path_sign_detached(Msg, Secret)},
       {5, no_keys_sign_detached(Msg, Secret)}]).

-spec invalid_sign(ercoin_sig:msg(), secret()) -> triq_dom:domain(binary()).
invalid_sign(Msg, Secret) ->
    ?LET(
       InvalidSig,
       invalid_sign_detached(Msg, Secret),
       <<Msg/binary, InvalidSig/binary>>).

-spec address_decomposed(secret()) -> binary().
address_decomposed({PKsSKs, Path}) ->
    list_to_binary(
      [length(PKsSKs),
       [PK || {PK, _} <- PKsSKs],
       merkle_proofs:'serialize-path'(Path)]).

-spec sign_detached(ercoin_sig:msg(), secret()) -> ercoin_sig:sig().
sign_detached(Msg, Secret) ->
    AddressDecomposed = address_decomposed(Secret),
    ToSign = <<Msg/binary, AddressDecomposed/binary>>,
    RawSigs = raw_sign_detached(ToSign, Secret),
    list_to_binary([AddressDecomposed, RawSigs]).

-spec raw_sign_detached(iodata(), secret()) -> binary().
raw_sign_detached(ToSign, {PKsSKs, _}) ->
    list_to_binary([enacl:sign_detached(ToSign, SK) || {_, SK} <- PKsSKs]).

-spec sign(ercoin_sig:msg(), secret()) -> binary().
sign(Msg, Secret) ->
    Signature = sign_detached(Msg, Secret),
    <<Msg/binary, Signature/binary>>.

prop_non_malleable_part_size() ->
    ?FORALL(
       {_, Sig},
       msg_sig(),
       begin
           <<SubsigCount, _/binary>> = Sig,
           byte_size(ercoin_sig:non_malleable_part(Sig)) =:= byte_size(Sig) - SubsigCount * 64
       end).

prop_address_is_unpacked_from_sig() ->
    ?FORALL(
       {Msg, {Address, Secret}},
       {msg(), address_secret()},
       Address =:= ercoin_sig:unpack_address(sign_detached(Msg, Secret))).

prop_sig_is_verified() ->
    ?FORALL(
       {Msg, Sig},
       msg_sig(),
       ercoin_sig:verify_subsigs(Sig, Msg)).

prop_valid_sig_is_unpacked() ->
    ?FORALL(
       {Msg, {Address, Secret}},
       {msg(), address_secret()},
       Address =:= ercoin_sig:verify_and_unpack_address(sign_detached(Msg, Secret), Msg)).

prop_invalid_sig_is_not_unpacked() ->
    ?FORALL(
       {Msg, InvalidSig},
       ?LET(
          {Msg, Secret},
          {msg(), secret()},
          {Msg, invalid_sign_detached(Msg, Secret)}),
       undefined =:= ercoin_sig:verify_and_unpack_address(InvalidSig, Msg)).
