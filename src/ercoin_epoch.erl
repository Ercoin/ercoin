%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_epoch).

-export_type(
   [stage/0]).

-export(
   [all_stages/0,
    stage/1,
    stage_no/1,
    stage_gte/2,
    progress/3]).

-include_lib("include/ercoin.hrl").

-type stage() :: beginning | drawing_frozen | drawing_yielded | drawing_to_be_announced | drawing_announced.
-type action() :: fun((data()) -> data()).

-define(
   STAGES,
   [beginning,
    drawing_frozen,
    drawing_yielded,
    drawing_to_be_announced,
    drawing_announced]).

-spec all_stages() -> list(stage()).
all_stages() ->
    ?STAGES.

-spec stage(data()) -> stage().
stage(#data{epoch_stage=Stage}) ->
    Stage.

-spec stage_no(data() | stage()) -> pos_integer().
stage_no(#data{epoch_stage=Stage}) ->
    ?FUNCTION_NAME(Stage);
stage_no(Stage) ->
    list_index(Stage, ?STAGES).

-spec list_index(term(), list(term())) -> pos_integer().
list_index(El, List) ->
    list_index(El, List, 1).

-spec list_index(term(), list(term()), pos_integer()) -> pos_integer().
list_index(El, [El|_], I) ->
    I;
list_index(El, [_|ListTail], I) ->
    list_index(El, ListTail, I+1).

-spec stage_gte(data() | stage(), stage()) -> boolean().
stage_gte(#data{epoch_stage=Stage1}, Stage2) ->
    stage_gte(Stage1, Stage2);
stage_gte(Stage1, Stage2) ->
    stage_no(Stage1) >= stage_no(Stage2).

-spec should_we_step(data()) -> boolean().
should_we_step(
  #data{
     epoch_stage=Stage,
     epoch_length=Length,
     last_epoch_end=LastEpochEnd,
     timestamp=Timestamp}) ->
    Duration = Timestamp - LastEpochEnd,
    case Stage of
        beginning ->
            Duration >= Length div 4;
        drawing_frozen ->
            Duration >= Length * 3 div 4;
        drawing_yielded ->
            Duration >= Length - ?BLOCK_LENGTH * 2;
        drawing_to_be_announced ->
            true;
        drawing_announced ->
            true
    end.

-spec execute_actions(list(action()), data()) -> data().
execute_actions([], Data) ->
    Data;
execute_actions([Action|ActionsTail], Data) ->
    execute_actions(ActionsTail, apply(Action, [Data])).

-spec progress(data(), non_neg_integer(), list({{ercoin_sig:address(), binary()}, boolean()})) -> data().
progress(Data=#data{epoch_stage=Stage}, LastBlockDuration, ValidatorsWithPresence) ->
    Data1 = increase_absencies(Data, LastBlockDuration, ValidatorsWithPresence),
    case should_we_step(Data1) of
        true ->
            NewStage = next_stage(Stage),
            Data2 = Data1#data{epoch_stage=NewStage},
            execute_actions(stage_actions(NewStage), Data2);
        false ->
            Data1
    end.

-spec next_stage(stage()) -> stage().
next_stage(drawing_announced) ->
    beginning;
next_stage(Stage) ->
    next_stage(Stage, ?STAGES).

-spec next_stage(stage(), list(stage())) -> stage().
next_stage(Stage, [Stage, NextStage|_]) ->
    NextStage;
next_stage(Stage, [_|TailStages]) ->
    next_stage(Stage, TailStages);
next_stage(_, []) ->
    beginning.

-spec update_last_epoch_end(data()) -> data().
update_last_epoch_end(Data=#data{previous_timestamp=PreviousTimestamp}) ->
    Data#data{last_epoch_end=PreviousTimestamp}.

-spec stage_actions(stage()) -> list(action()).
stage_actions(beginning) ->
    [fun ercoin_data:grant_fee_deposit/1,
     fun update_last_epoch_end/1,
     fun ercoin_validators:set_future_as_current/1];
stage_actions(drawing_frozen) ->
    [fun ercoin_validators:begin_drawing/1];
stage_actions(drawing_yielded) ->
    [fun ercoin_validators:yield_drawing/1];
stage_actions(drawing_to_be_announced) ->
    [];
stage_actions(drawing_announced) ->
    [].

-spec increase_absencies(data(), non_neg_integer(), list({{ercoin_sig:address(), binary()}, boolean()})) -> data().
increase_absencies(Data=#data{validators=Validators}, TimestampDiff, ValidatorsWithPresence) ->
    NewValidators =
        lists:foldl(
          fun ({_, true}, Acc) ->
                  Acc;
              ({{Address, <<Power, Absencies:3/unit:8, VoteBin/binary>>}, false}, Acc) ->
                  NewAbsencies = Absencies + TimestampDiff,
                  gb_merkle_trees:enter(Address, <<Power, NewAbsencies:3/unit:8, VoteBin/binary>>, Acc)
          end,
          Validators,
          ValidatorsWithPresence),
    Data#data{validators=NewValidators}.
