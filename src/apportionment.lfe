;; Licensed under the Apache License, Version 2.0 (the “License”);
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an “AS IS” BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; For discussion of various apportionment schemes, see http://rangevoting.org/Apportion.html .

(defmodule hare-niemeyer
  (export (apportion 2)
          (apportion 3)))

(include-lib "include/apportionment.lfe")

(defspec (add-singular-seats-and-flip 2)
  (((list (tuple (pos_integer) (any))) (list (any))) (list (result))))
(defun add-singular-seats-and-flip (results beneficients)
  (++
   (lists:map
    (match-lambda
      (((tuple seats id))
       (case (lists:member id beneficients)
         ('false (tuple id seats))
         ('true (tuple id (+ 1 seats))))))
    results)
   (lc ((<- id (-- beneficients
                   (lc ((<- (tuple _ id-existing) results))
                     id-existing))))
     (tuple id 1))))

(defspec (apportion 4)
  (((list (score)) (pos_integer) (pos_integer) (rational:fraction)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores total-seats _ quota)
  (let* (((tuple int-results part-results int-seats-sum)
          (lists:foldl
           (match-lambda
             (((tuple score id) (tuple int-results-acc part-results-acc int-seats-sum))
              (let* ((fraction-seats (rational:divide (rational:new score) quota))
                     (int-seats (div (rational:numerator fraction-seats) (rational:denominator fraction-seats)))
                     (part-seats (rational:subtract fraction-seats (rational:new int-seats))))
                (tuple
                 (case int-seats
                   (0 int-results-acc)
                   (_ (cons (tuple int-seats id) int-results-acc)))
                 (cons (tuple part-seats id) part-results-acc)
                 (+ int-seats int-seats-sum)))))
           (tuple '() '() 0)
           scores))
         (part-seats-for
          (lists:map
           (match-lambda (((tuple _ id)) id))
           (lists:sublist
            (lists:sort (match-lambda (((tuple score_x id_x) (tuple score_y id_y))
                                       (case (rational:is_equal_to score_x score_y)
                                         ('true (>= id_x id_y))
                                         ('false (rational:is_greater_than score_x score_y)))))
                        part-results)
            (- total-seats int-seats-sum)))))
    (lists:sort (add-singular-seats-and-flip int-results part-seats-for))))

(defspec (apportion 3)
  (((list (score)) (non_neg_integer) (pos_integer)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores seats votes-sum)
  (case seats
    (0 '())
    (_ (apportion scores seats votes-sum (rational:new votes-sum seats)))))

(defspec (apportion 2)
  (((list (score)) (non_neg_integer)) (list (tuple (any) (pos_integer)))))
(defun apportion (scores seats)
  (let ((total-votes (lists:foldl (match-lambda (((tuple votes _) acc) (+ votes acc))) 0 scores)))
    (apportion scores seats total-votes)))
