-module(random_apportionment).

-export(
   [apportion/2,
    apportion/3,
    apportion/4]).

-type score() :: ({Votes :: non_neg_integer(), Id :: any()}).
-type result() :: {Id :: any(), Seats :: non_neg_integer()}.

-spec apportion(list(score()), pos_integer()) -> list(result()).
apportion(Scores, TotalSeats) ->
    apportion(
      Scores,
      TotalSeats,
      lists:foldl(
        fun ({Votes, _}, Acc) -> Acc + Votes end,
        0,
        Scores)).

-spec apportion(list(score()), pos_integer(), pos_integer()) -> list(result()).
apportion(Scores, TotalSeats, VotesSum) ->
    apportion(Scores, TotalSeats, VotesSum, crypto:strong_rand_bytes(32)).

-spec apportion(list(score()), pos_integer(), pos_integer(), binary()) -> list(result()).
apportion(Scores, TotalSeats, VotesSum, State) ->
    {WholeSeatsMap, RemainderScores, RemainingSeats, _} =
        lists:foldl(
          fun (
            {Votes, Id},
            {WholeSeatsAcc,
             RemainderScoresAcc,
             RemSeatsAcc,
             StateAcc}) ->
                  Seats = Votes * TotalSeats div VotesSum,
                  <<RandInt:32, _/binary>> = NewState = crypto:hash(sha256, StateAcc),
                  RemainderScore = (Votes * TotalSeats rem VotesSum) * (RandInt + 1),
                  {maps:put(Id, Seats, WholeSeatsAcc),
                   [{RemainderScore, Id}|RemainderScoresAcc],
                   RemSeatsAcc - Seats,
                   NewState}
          end,
          {#{},
           [],
           TotalSeats,
           State},
          Scores),
    DrawnIds =
        %% We actually need to sort the list only at first RemainderSeats places, so here is a room for optimization.
        [Id || {_, Id} <- lists:sublist(lists:reverse(lists:sort(RemainderScores)), RemainingSeats)],
    ResultMap =
        lists:foldl(
          fun (Id, ResultMapAcc) ->
                  maps:update_with(
                    Id,
                    fun (OldSeats) -> OldSeats + 1 end,
                    1,
                    ResultMapAcc)
          end,
          WholeSeatsMap,
          DrawnIds),
    lists:sort([{Id, Seats} || {Id, Seats} <- maps:to_list(ResultMap), Seats > 0]).
