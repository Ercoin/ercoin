%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_tx).

-compile({parse_transform, dynarec}).
-compile({parse_transform, category}).

-include_lib("include/ercoin.hrl").
-include_lib("include/ercoin_tx.hrl").

-export(
   [apply/2,
    default_age_margin/1,
    deserialize/1,
    error_code/2,
    error_code/3,
    events/2,
    from/1,
    serialize/1,
    serialize_non_malleable_part/1,
    unpack_binary/2,
    unpack_binary/3,
    handle_bin/2,
    handle_bin/3,
    required_balance/2,
    timestamp/1,
    value/1]).

-export_type([age_margin/0]).
-export_type([tx/0]).

-import(
   datum_cat_option,
   [fail/1,
    unit/1]).

-type age_margin() :: non_neg_integer().

-spec timestamp(tx()) -> datum:option(ercoin_timestamp:timestamp()).
timestamp(#transfer_tx{timestamp=Timestamp}) ->
    Timestamp;
timestamp(#vote_tx{vote=#vote{timestamp=Timestamp}}) ->
    Timestamp;
timestamp(#burn_tx{timestamp=Timestamp}) ->
    Timestamp;
timestamp(_) ->
    undefined.

-spec from(tx()) -> ercoin_sig:address().
from(#transfer_tx{from=From}) ->
    From;
from(#lock_tx{address=Address}) ->
    Address;
from(#vote_tx{address=Address}) ->
    Address;
from(#burn_tx{address=Address}) ->
    Address.

-spec to(tx()) -> datum:option(ercoin_sig:address()).
to(#transfer_tx{to=To}) ->
    To;
to(_) ->
    undefined.

-spec message(tx()) -> datum:option(message()).
message(Tx) when is_record(Tx, burn_tx); is_record(Tx, transfer_tx) ->
    get_value(message, Tx);
message(_) ->
    undefined.

-spec value(tx()) -> money().
value(Tx) when is_record(Tx, burn_tx); is_record(Tx, transfer_tx) ->
    get_value(value, Tx);
value(_) ->
    0.

-spec type_no(tx()) -> non_neg_integer().
type_no(Tx) ->
    <<TypeNo, _/binary>> = serialize(Tx),
    TypeNo.

-spec required_balance(tx(), data()) -> non_neg_integer().
required_balance(Tx, Data) ->
    value(Tx) + ercoin_fee:fee(Tx, Data).

-spec serialize(tx()) -> binary().
serialize(
  #transfer_tx{
     timestamp=Timestamp,
     to=To,
     value=Value,
     message=Message,
     signature=Signature}) ->
    MessageLength = byte_size(Message),
    <<0, Timestamp:4/unit:8, To/binary, Value:8/unit:8, MessageLength, Message/binary, Signature/binary>>;
serialize(
 #lock_tx{
    locked_until=LockedUntil,
    validator_pk=ValidatorPK,
    signature=Signature}) ->
    <<2, LockedUntil:4/unit:8, ValidatorPK/binary, Signature/binary>>;
serialize(
 #vote_tx{
    vote=
        #vote{
           timestamp=Timestamp,
           choices=Choices},
    signature=Signature}) ->
    ChoicesBin = ercoin_vote:choices_serialize(Choices),
    <<3, Timestamp:4/unit:8, ChoicesBin/binary, Signature/binary>>;
serialize(
  #burn_tx{
     timestamp=Timestamp,
     value=Value,
     message=Message,
     signature=Signature}) ->
    MsgLength = byte_size(Message),
    <<4, Timestamp:4/unit:8, Value:8/unit:8, MsgLength, Message/binary, Signature/binary>>.

-spec deserialize_1(binary()) -> datum:option(tx()).
%% TODO: Do not construct records with potentially invalid address/from fields. A good idea may be to unify many transaction records into one data structure.
deserialize_1(<<0, Timestamp:4/unit:8, To:32/binary, Value:8/unit:8, MsgLength/integer, Msg:MsgLength/binary, Signature/binary>>) ->
    unit(
      #transfer_tx{
         timestamp=Timestamp,
         from=ercoin_sig:unpack_address(Signature),
         to=To,
         value=Value,
         message=Msg,
         signature=Signature});
deserialize_1(<<2, LockedUntil:4/unit:8, ValidatorPK:32/binary, Signature/binary>>) ->
    unit(
      #lock_tx{
         locked_until=LockedUntil,
         address=ercoin_sig:unpack_address(Signature),
         validator_pk=ValidatorPK,
         signature=Signature});
deserialize_1(<<3, Timestamp:4/unit:8, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol, Signature/binary>>) ->
    unit(
      #vote_tx{
         address=ercoin_sig:unpack_address(Signature),
         vote=
             #vote{
                timestamp=Timestamp,
                choices=#{
                          fee_per_tx => FeePerTx,
                          fee_per_256_bytes => FeePer256B,
                          fee_per_account_day => FeePerAccountDay,
                          protocol => Protocol}},
         signature=Signature});
deserialize_1(<<4, Timestamp:4/unit:8, Value:8/unit:8, MsgLength, Msg:MsgLength/binary, Signature/binary>>) ->
    unit(
      #burn_tx{
         timestamp=Timestamp,
         address=ercoin_sig:unpack_address(Signature),
         value=Value,
         message=Msg,
         signature=Signature});
deserialize_1(_) ->
    fail("Invalid format.").

-spec deserialize(binary()) -> datum:option(tx()).
deserialize(TxBin) ->
    [option ||
        Tx <- deserialize_1(TxBin),
        Signature =< get_value(signature, Tx),
        SignedMsg =< binary:part(TxBin, 0, byte_size(TxBin) - byte_size(Signature)),
        cats:require(
          %% The first check won’t be necessary when deserialize_1 stops emitting incorrect records.
          is_binary(from(Tx)) andalso ercoin_sig:verify_subsigs(Signature, SignedMsg),
          Tx,
          "Invalid subsigs.")].

-spec serialize_non_malleable_part(tx()) -> binary().
serialize_non_malleable_part(Tx) ->
    serialize(
      set_value(
        signature,
        ercoin_sig:non_malleable_part(get_value(signature, Tx)),
        Tx)).

-spec default_age_margin(data()) -> age_margin().
default_age_margin(Data) ->
    ercoin_data:get_value(epoch_length, Data).

-spec error_code(tx(), data()) -> error_code().
error_code(Tx, Data) ->
    error_code(Tx, Data, default_age_margin(Data)).

-spec error_code(tx(), data(), age_margin()) -> error_code().
error_code(Tx, Data=#data{timestamp=Timestamp}, AgeMargin) ->
    TxTimestamp = ercoin_tx:timestamp(Tx),
    case TxTimestamp =:= undefined orelse TxTimestamp =< Timestamp + AgeMargin andalso TxTimestamp >= Timestamp - AgeMargin of
        true ->
            case is_record(Tx, vote_tx) of
                true ->
                    error_code_1(Tx, Data, none);
                false ->
                    case ercoin_account:get(ercoin_tx:from(Tx), Data) of
                        none ->
                            ?NOT_FOUND;
                        From=#account{balance=Balance} ->
                            case Balance >= ercoin_tx:required_balance(Tx, Data) of
                                true ->
                                    error_code_1(Tx, Data, From);
                                false ->
                                    ?INSUFFICIENT_FUNDS
                            end
                    end
            end;
        _ ->
            ?INVALID_TIMESTAMP
    end.

-spec error_code_1(tx(), data(), account() | none) -> error_code().
error_code_1(Tx=#transfer_tx{}, Data, #account{locked_until=LockedUntil}) ->
    case in_fresh_txs(Tx, Data) of
        false ->
            case LockedUntil of
                none ->
                    ?OK;
                _ ->
                    ?FORBIDDEN
            end;
        true ->
            ?ALREADY_EXECUTED
    end;
error_code_1(
  #lock_tx{locked_until=LockedUntil}, #data{timestamp=Timestamp}, #account{locked_until=CurrentLockedUntil}) ->
    case LockedUntil > Timestamp of
        true ->
            case CurrentLockedUntil =:= none orelse LockedUntil > CurrentLockedUntil of
                true ->
                    ?OK;
                false ->
                    ?FORBIDDEN
            end;
        false ->
            ?INVALID_TIMESTAMP
    end;
error_code_1(Tx=#vote_tx{address=Address, vote=#vote{timestamp=Timestamp}}, Data, _) ->
    case in_fresh_txs(Tx, Data) of
        false ->
            case ercoin_vote:lookup(Address, Data) of
                validator_not_found ->
                    ?NOT_FOUND;
                none ->
                    ?OK;
                #vote{timestamp=CurrentTimestamp} ->
                    case CurrentTimestamp < Timestamp of
                        true ->
                            ?OK;
                        false ->
                            ?INVALID_TIMESTAMP
                    end
            end;
        _ ->
            ?ALREADY_EXECUTED
    end;
error_code_1(Tx=#burn_tx{}, Data, #account{locked_until=LockedUntil}) ->
    case in_fresh_txs(Tx, Data) of
        false ->
            case LockedUntil of
                none ->
                    ?OK;
                _ ->
                    ?FORBIDDEN
            end;
        true ->
            ?ALREADY_EXECUTED
    end.

-spec in_fresh_txs(tx(), data()) -> boolean().
in_fresh_txs(Tx, Data) ->
    ?SETS:is_element({ercoin_tx:timestamp(Tx), ?HASH(ercoin_tx:serialize_non_malleable_part(Tx))}, Data#data.fresh_txs).

-spec unpack_binary(binary(), data()) -> {error_code(), tx() | none}.
unpack_binary(MaybeTxBin, Data) ->
    unpack_binary(MaybeTxBin, Data, default_age_margin(Data)).

-spec unpack_binary(binary(), data(), age_margin()) -> {error_code(), tx() | none}.
unpack_binary(MaybeTxBin, Data, AgeMargin) ->
    case deserialize(MaybeTxBin) of
        undefined ->
            {?BAD_REQUEST, none};
        Tx ->
            case error_code(Tx, Data, AgeMargin) of
                ?OK ->
                    {?OK, Tx};
                ErrorCode ->
                    {ErrorCode, none}
            end
    end.

-spec handle_bin(binary(), data()) -> {error_code(), data()}.
handle_bin(MaybeTxBin, Data) ->
    handle_bin(MaybeTxBin, Data, default_age_margin(Data)).

-spec handle_bin(binary(), data(), age_margin()) -> {error_code(), data()}.
handle_bin(MaybeTxBin, Data, AgeMargin) ->
    case unpack_binary(MaybeTxBin, Data, AgeMargin) of
        {?OK, Tx} ->
            {?OK, ercoin_tx:apply(Tx, Data)};
        {Error, none} ->
            {Error, Data}
    end.

-spec apply(tx(), data()) -> data().
apply(Tx, Data=#data{fee_deposit=FeeDeposit}) ->
    Fee = ercoin_fee:fee(Tx, Data),
    case Fee of
        0 ->
            apply_1(Tx, Data);
        _ ->
            From = ercoin_account:get(ercoin_tx:from(Tx), Data),
            NewFeeDeposit = FeeDeposit + Fee,
            NewFrom = From#account{balance=From#account.balance - Fee},
            apply_1(
              Tx,
              ercoin_account:put(
                NewFrom,
                Data#data{fee_deposit=NewFeeDeposit}))
    end.

-spec apply_1(tx(), data()) -> data().
apply_1(Tx=#transfer_tx{to=To, from=From, value=Value}, Data) ->
    FromAccount = ercoin_account:get(From, Data),
    NewFromAccount = FromAccount#account{balance=FromAccount#account.balance - Value},
    Data1 = ercoin_account:put(NewFromAccount, Data),
    Data2 = ercoin_account:add_balance(To, Value, Data1),
    add_to_fresh_txs(Tx, Data2);
apply_1(#lock_tx{locked_until=LockedUntil, address=Address, validator_pk=ValidatorPK}, Data) ->
    Account = ercoin_account:get(Address, Data),
    ercoin_account:put(Account#account{locked_until=LockedUntil, validator_pk=ValidatorPK}, Data);
apply_1(Tx=#vote_tx{vote=Vote, address=Address}, Data) ->
    Data1 = ercoin_vote:put(Address, Vote, Data),
    add_to_fresh_txs(Tx, Data1);
apply_1(Tx=#burn_tx{address=Address, value=Value}, Data) ->
    Account = ercoin_account:get(Address, Data),
    NewAccount = Account#account{balance=Account#account.balance - Value},
    Data1 = ercoin_account:put(NewAccount, Data),
    add_to_fresh_txs(Tx, Data1).

-spec add_to_fresh_txs(tx(), data()) -> data().
%% @doc Add a transaction to fresh_txs and to fresh_txs_hash.
add_to_fresh_txs(Tx, Data=#data{fresh_txs=FreshTxs, fresh_txs_hash=FreshTxsHash}) ->
    TxPartBin = ercoin_tx:serialize_non_malleable_part(Tx),
    NewFreshTxs = ?SETS:add_element({ercoin_tx:timestamp(Tx), ?HASH(TxPartBin)}, FreshTxs),
    NewFreshTxsHash = ?HASH(<<FreshTxsHash/binary, TxPartBin/binary>>),
    Data#data{fresh_txs=NewFreshTxs, fresh_txs_hash=NewFreshTxsHash}.

-spec events(tx() | binary(), data()) -> list(ercoin_event:event()).
events(TxBin, Data) when is_binary(TxBin) ->
    events(deserialize(TxBin), Data);
events(Tx, Data) ->
    MaybeAttrs =
        #{<<"fee">> => integer_to_binary(ercoin_fee:fee(Tx, Data)),
          <<"from">> => base64:encode(from(Tx)),
          <<"message">> => [option || message(Tx), base64:encode(_)],
          <<"to">> => [option || to(Tx), base64:encode(_)],
          <<"type">> => integer_to_binary(type_no(Tx)),
          <<"value">> => integer_to_binary(value(Tx))},
    Attrs = maps:filter(fun(_Key, Value) -> is_binary(Value) end, MaybeAttrs),
    [#{type => <<"tx">>,
       attributes => Attrs}].
