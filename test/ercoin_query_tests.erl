%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_query_tests).

-include_lib("abci_server/include/abci.hrl").
-include_lib("include/ercoin_test.hrl").

-compile({parse_transform, dynarec}).

-import(
   ercoin_query_gen,
   [data_with_query_account_not_existing/0,
    data_with_query_account_valid/0,
    data_with_query_fees/0,
    data_with_query_unknown/0]).

prop_account_query_returns_serialized_account() ->
    ?FORALL(
       {Data, Query=#'tendermint.abci.types.RequestQuery'{data=Address}},
       data_with_query_account_valid(),
       begin
           Account = ercoin_account:get(Address, Data),
           {Address, Value} = ercoin_account:serialize(Account),
           #'tendermint.abci.types.ResponseQuery'{value=ResponseValue} =
               ercoin_query:perform(Query, Data),
           Value =:= ResponseValue
       end).

prop_account_query_returns_not_found_code_for_non_existing_account() ->
    ?FORALL(
       {Data, Query},
       data_with_query_account_not_existing(),
       ?NOT_FOUND =:= get_value(code, ercoin_query:perform(Query, Data))).

prop_unknown_query_returns_bad_request_code() ->
    ?FORALL(
       {Data, Query},
       data_with_query_unknown(),
       ?BAD_REQUEST =:= get_value(code, ercoin_query:perform(Query, Data))).

prop_fees_query_returns_fees() ->
    ?FORALL(
       {Data, Query},
       data_with_query_fees(),
       begin
           #'tendermint.abci.types.ResponseQuery'{value=ResponseValue, code=ResponseCode} =
               ercoin_query:perform(Query, Data),
           ResponseCode =:= ?OK andalso ResponseValue =:= ercoin_fee:serialize(ercoin_fee:fees(Data))
       end).
