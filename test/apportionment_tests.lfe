;; Licensed under the Apache License, Version 2.0 (the “License”);
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an “AS IS” BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(defmodule apportionment_tests
  (export all))

(include-lib "lfe/include/clj.lfe")
(include-lib "triq/include/triq.hrl")
(include-lib "include/apportionment.lfe")

(defun score ()
  (tuple (non_neg_integer) (atom)))

(defun seats ()
  (choose 0 (- (bsl 1 64) 1)))

(defspec (proplists_remove_value_duplicates 1)
  (((list (tuple (any) (any)))) (list (tuple (any) (any)))))
(defun proplists_remove_value_duplicates (proplist)
  (let (((tuple result _)
         (lists:foldr
          (match-lambda
            (((tuple key value) (tuple result-acc used))
             (case (sets:is_element value used)
               ('true (tuple result-acc used))
               ('false (tuple (cons (tuple key value) result-acc) (sets:add_element value used))))))
          (tuple (list) (sets:new))
          proplist)))
    result))

(defun scores ()
  (SUCHTHAT
   scores
   (LET
    unordered-scores
    (triq_dom:list (score))
    (proplists_remove_value_duplicates unordered-scores))
   (lists:any (match-lambda (((tuple votes _)) (> votes 0))) scores)))

(defspec (sum-scores 1)
  (((list (score))) (pos_integer)))
(defun sum-scores (scores)
  (lists:foldl (match-lambda (((tuple votes _) acc) (+ votes acc))) 0 scores))

(defun random-apportionment-method ()
  (noshrink
   (LET
   seed
     (tuple (int) (int) (int))
     (let ((state (crypto:strong_rand_bytes 32)))
       (lambda (scores seats) (random_apportionment:apportion scores seats (sum-scores scores) state))))))

(defun method ()
  (oneof
   (list
    (function hare-niemeyer apportion 2)
    (random-apportionment-method))))

(defspec (rational_floor 1)
  (((rational:fraction)) (integer)))
(defun rational_floor (fraction)
  (div (rational:numerator fraction)
       (rational:denominator fraction)))

(defspec (rational_ceil 1)
  (((rational:fraction)) (integer)))
(defun rational_ceil (fraction)
  (case (rem (rational:numerator fraction)
             (rational:denominator fraction))
    (0 (div (rational:numerator fraction)
            (rational:denominator fraction)))
    (_ (+ 1
          (div (rational:numerator fraction)
               (rational:denominator fraction))))))

(defun prop_quota ()
  "Every participant gets number of seats equal either to the floor or ceil of his share multiplied by total seats."
  (FORALL
   (tuple scores total-seats method)
   (tuple (scores) (seats) (oneof (list (function hare-niemeyer apportion 2) (random-apportionment-method))))
   (let ((results (apply method (list scores total-seats))))
     (lists:all
      (match-lambda
        (((tuple score id))
         (let ((seats (proplists:get_value id results 0))
               (expected (rational:multiply (rational:new score (sum-scores scores)) (rational:new total-seats))))
           (orelse (== seats (rational_ceil expected)) (== seats (rational_floor expected))))))
      scores))))

(defun prop_seats-sum ()
  "Apportioned seats sum to the desired number of total seats."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (seats) (method))
   (=:= seats
        (lists:foldl
         (match-lambda (((tuple _ seats) acc) (+ seats acc)))
         0
         (apply method (list scores seats))))))

(defun prop_uniqueness ()
  "Apportioned seats are unique by id."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (seats) (method))
   (let ((results (apply method (list scores seats))))
     (=:= (length results)
          (sets:size (sets:from_list (lc ((<- (tuple id _) results)) id)))))))

(defun prop_result_ordering ()
  "Apportioned seats are sorted."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (seats) (method))
   (let ((results (apply method (list scores seats))))
     (=:= results (lists:sort results)))))

(defun prop_score_ordering ()
  "In case of equal scores, problematic seats get assigned to one with higher id."
  (FORALL
   (tuple scores seats method)
   (tuple (scores) (seats) (function hare-niemeyer apportion 2))
   (let ((results (apply method (list scores seats))))
     (lists:all
      (match-lambda
        (((tuple points id1))
         (let ((result1 (proplists:get_value id1 results 0)))
           (lists:all
            (lambda
                (id2)
                 (let ((result2 (proplists:get_value id2 results 0)))
                   (case (> id2 id1)
                     ('true (>= result2 result1))
                     ('false (=< result2 result1)))))
            (proplists:get_all_values points scores)))))
      scores))))
